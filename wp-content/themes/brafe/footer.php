<?php $uri = get_template_directory_uri();?>
<footer>
        <section class="container">
            <div class="footer-text">
                <p>Este é um projeto da Origamid. Mais em <a href="https://www.origamid.com/">origamid.com</a></p>
                <address>Praia de Botafogo, 300, 5º andar - Botafogo - Rio de Janeiro</address>
            </div>
            <div class="">
                <img class="main-logo" src="<?php echo $uri; ?>/img/brafe.png" alt="Logo Brafé">
            </div>
        </section>
    </footer>
</body>
</html>