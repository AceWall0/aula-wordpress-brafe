<?php 
// Template Name: Home Page
?>
<?php get_header(); ?>

<main>
    <?php $uri = get_template_directory_uri();?>
        <section id="intro" style="background-image: url(<?php the_field('background1'); ?>);">
            <div>
                <h1><?php the_field('titulo_chamativo'); ?></h1>
                <h3>
                    <i><?php the_field('sub-titulo_chamativo'); ?></i>
                </h3>
            </div>
        </section>

        <section id="sobre">
            <?php $sobrePage =  get_page_by_title('sobre');?>
            <div class="container">
                <h2><?php the_field('titulo_sobre', $sobrePage); ?></h2>
                <div class="sobre-fotos">
                    <figure>
                        <img src="<?php the_field('sobre_foto1', $sobrePage); ?>" alt="Foto de café com leite">
                        <figcaption><?php the_field('legenda1', $sobrePage); ?></figcaption>
                    </figure>
                    <figure>
                        <img src="<?php the_field('sobre_foto2', $sobrePage); ?>" alt="Foto de café com leite">
                        <figcaption><?php the_field('legenda2', $sobrePage); ?></figcaption>
                    </figure>
                </div>
                <div class="sobre-texto">
                    <p><?php the_field('texto_sobre', $sobrePage); ?></p>
                </div>
            </div>
        </section>

        <section id="produtos">
            <?php $produtosPage = get_page_by_title('produtos');?>
            <div class="lista-cafe container">
                <div class="item-cafe">
                    <div class="circle paulista"></div>
                    <h2><?php the_field('produto_nome1', $produtosPage);?></h2>
                    <p><?php the_field('produto_texto1', $produtosPage);?></p>
                </div>
                <div class="item-cafe">
                    <div class="circle carioca"></div>
                    <h2><?php the_field('produto_nome2', $produtosPage);?></h2>
                    <p><?php the_field('produto_texto1', $produtosPage);?></p>
                </div>
                <div class="item-cafe">
                    <div class="circle mineiro"></div>
                    <h2><?php the_field('produto_nome3', $produtosPage);?></h2>
                    <p><?php the_field('produto_texto1', $produtosPage);?></p>
                </div>
            </div>
            <button id="saibaMais" class="btn btn-transparent">SAIBA MAIS</button>
        </section>

<!-- Vou fazer o resto editável depois ou talvez nem faça pra fazer as outras tarefas. O resto a partir da qui é pura
repetição e no momento eu to assistindo a sua aula 3 de wordpress kkk -->

        <section id="portfolio">
            <div class="container">
                <div class="portfolio-card">
                    <img src="<?php echo $uri; ?>/img/botafogo.jpg" alt="Cafeteria em Botafogo">
                    <h2>Botafogo</h2>
                    <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                    <button class="btn btn-tight btn-transparent">VER MAPA</button>
                </div>
                <div class="portfolio-card">
                    <img src="<?php echo $uri; ?>/img/iguatemi.jpg" alt="Cafeteria em Iguatemi">
                    <h2>Iguatemi</h2>
                    <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.
                    </p>
                    <button class="btn btn-tight btn-transparent">VER MAPA</button>
                </div>
                <div class="portfolio-card">
                    <img src="<?php echo $uri; ?>/img/mineirao.jpg" alt="Cafeteria em Mineirão">
                    <h2>Mineirão</h2>
                    <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                    <button class="btn btn-tight btn-transparent">VER MAPA</button>
                </div>
            </div>
        </section>

        <section id="contato">
            <div class="container">
                <div class="newsletter-title">
                    <h2>Assine Nossa Newsletter</h2>
                    <h3>promoções e eventos mensais</h3>
                </div>

                <form action="./">
                    <input class="input-box" type="email" name="email" id="email" placeholder="Digite seu email">
                    <input class="btn btn-dark btn-tighter" type="submit" value="ENVIAR">
                </form>
            </div>
        </section>
    </main>

<?php get_footer(); ?>