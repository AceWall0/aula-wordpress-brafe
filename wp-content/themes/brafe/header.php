<?php $uri = get_template_directory_uri();?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=
    , initial-scale=1.0">
    <title>Brafé <?php echo wp_title()?> </title>
    <?php wp_head();
    
    $args = array(
        'menu' => 'principal',
        'container_class' => 'menu',
        'container' => 'nav'
    );
    ?>
</head>
<body>
    <header>
        <div class="container">
            <a href="<?php 
                if (is_front_page()) {
                    echo "#";
                    } 
                    else {
                        echo "/";
                    } 
            ?>"><img class="main-logo" src="<?php echo $uri; ?>/img/brafe.png" alt="Logo Brafé"></a>

            <?php wp_nav_menu($args);?>
        </div>
    </header>