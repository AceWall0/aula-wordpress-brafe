<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UCbUyMBALspcMLGc+OIOYbbLGKd6SUgV8w40IQtwt4qxUPXkU6zFpe5X/jB/R0Uolc9B78mq1XZL1B3dL8VEjQ==');
define('SECURE_AUTH_KEY',  'kjcPEHAWDQc37UXQKAO7xDX5mcGjjTAirbjHWqOUcR+T20gOdPDA4OlH0Sn3aypP1eNeb/F1dh/ZOfhEyHGbBQ==');
define('LOGGED_IN_KEY',    '0nLu08IzX0esJcUql/psjWZZGPrjkzzNJhpKqu6ZDAnmQZPJYfDTcFQd9kjh4SnPcS3JYAbFOcUSRx6fuO83Vg==');
define('NONCE_KEY',        'LeV0jmmrcTABExY1H1GpnbfU+8+9H1R2AbrqNqBpcDQuRYRKYXiNowwbpJHo1K+2QdedKHDm6srAEqkKMQM8ow==');
define('AUTH_SALT',        'OjqN4BvcXY1odml1XCiqdrdjlpVL8Y22tz77utTcjJk280rtKNICDiu57zDKs2bkkWFywu/7DjZ2LUq+yF1h/w==');
define('SECURE_AUTH_SALT', 'muPUpnRWsQUmG6wvxTAfcuMBJ/flTqnMRCNWKDIHY4bVcuiCzDX8YtKkJPCqTOOdx6UWfpdtfIuOA+5SjommtQ==');
define('LOGGED_IN_SALT',   'SdWWGCCv2l0KArn4pHGR8G9fFP9tbq/kL8vHJXh7n8ifKpRr9MBlCsbzh/83FAQcKtnkA+vkdJd7fGI5OJbeSw==');
define('NONCE_SALT',       'eeiCFK1B4OTfsYd+Kbwuy6ipVsxA4hxh6W3v413eq63fwISyXYQG+pI+4iLBGfBdMda3W6hcUcb0i4NedvGWCQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
